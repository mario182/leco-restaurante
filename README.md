# Restaurante LECO

Este proyecto sirve para generar y visualizar comandas en el Restaurante LECO.

## Prerequisitos para ejecutar

1 - Instalar Node js y NPM https://nodejs.org/en/download/

2 - Instalar MongoDB https://docs.mongodb.com/v3.2/administration/install-community/

## Estructura

### BackEnd
	
En la carpeta backend se encuentra el servidor de la aplicación. Esta compuesto de los siguientes archivos:

-backend
|
|_build/							Contiene el frontend de la aplicación (las vistas).
|_controladores/					Controladores para cada entidad.
|_model/							Modelo de las entidades. 
|_repository/						Conexión a BD.
|_application.properties			Archivo con las configuraciones de la aplicación.
|_servidor.js						El archivo que inicia el servidor.
|_cenas.json						_
|_comidas.json						 |_Archivos json que contienen un menú default.
|_desayunos.json					_|

####Configuración

En el archivo application.properties se puede configurar los datos de conexión a la BD, 
archivos con el menú inicial y clasificaciones para los platillos.
	
	database.host= 127.0.0.1   
	database.port= 27017
	database.dbName= restaurante
	
	data.files= ./desayunos.json,./comidas.json,./cenas.json  --> Inicialmente se esta apuntando a los archivos con el menú default
																  Se puede poner cuantos archivos sean necesarios separados por una coma.
																  Los datos en dichos archivos se cargarán a la BD durante el arranque de
																  la aplicación.
	menu.categorias=Desayuno,Comida,Cena  --> Son las categorías para dividir el menú.
	menu.tipos=Entradas,Principales,Bebidas --> Los tipos de platillos para dividir una categoría.
												CUIDADO! Si mueve las categorías o los tipos deberá actualizar el menú.
### FrontEnd

El frontend se encuentra en la carpeta 'cliente'. Esta desarrollado con REACT.

La estructura de widgets es la siguiente:

- index.js					<- Componente principal.
	- Restaurante.js		<- Componente con la lógica del restaurante.
		- Menu.js			<- Componente con lógica para desplegar el menú y generar una nueva comanda.
			-Platillo.js	<- Visualización de un platillo con lógica para adicionarlo a una comanda.
			-Comanda.js		<- Visualización de una comanda (en el widget 'Menu' es editable).
		- Comandas.js		<- Componente con la lista de las comandas creadas.
			-Comanda.js		<- Visualización de una comanda (en el widget 'Comandas' no es editable).

##Ejecutar el proyecto

1- Abrir una consola o termial y navegar al folder del backend 
	
	cd /direccion-de-instalacion/leco-restaurante/backend

2- Ejecutar el archivo servidor.js con node
	
	node servidor.js

El servidor arrancará e inmediatamente cargara los datos del menú almacenados en los json si es que no existen en la BD.
Una vez finalizado el proceso se verá la leyenda 'Servidor iniciado'

Abrir en un browser el url 'http://localhost:8080'.