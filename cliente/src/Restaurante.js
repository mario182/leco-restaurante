import React, { Component } from 'react';
import axios from 'axios';
import Menu  from './Menu.js';
import Comandas  from './Comandas.js';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Container , Tabs, Navbar, Content} from 'react-bulma-components/full';

import './Restaurante.css';

const apiUrl = 'http://localhost:3000/';

class Restaurante extends Component {

	constructor() {
		super();
		this.state = { nueva: true,
			tipos : [] , 
			categorias : [], 
			menu : [], 
			ordenes: [],
			categoriaSelected:'',
      		platillosComanda : []
		};
	}

	changeTab(param){
		this.setState({nueva : param});
	}
	
	componentDidMount() {
		this.buscaMenu();
		this.buscaComandas();
	}

	buscaMenu(){
		axios.get(apiUrl + 'menu').then(res => {
			const tipos = res.data.tipos.map(obj => obj);
			const categorias = res.data.categorias.map(obj => obj);
			const menu = res.data.menu;
			this.setState({ tipos : tipos, 
			  categorias : categorias, 
			  menu : menu, 
			  categoriaSelected: categorias[0]
			});
		});
	}

	buscaComandas(){
		axios.get(apiUrl + 'ordenes')
		.then(res => {
		  this.setState({ 
			  ordenes : res.data
		  });
		});
	}

	filtraCategoria(e){
		this.setState({categoriaSelected:e});
	}

	enviaComanda(){
		var comanda ={
		  fecha : Math.floor(Date.now() / 1000),
		  platillos : this.state.platillosComanda
		};
		axios.post(apiUrl + 'crearOrden', comanda).then(res => {
		  toast("Comanda creada con éxito!");
		  var array = this.state.ordenes;
		  array.unshift(comanda);
		  this.setState({platillosComanda: [], ordenes: array});
		}); 
	}

	adicionaPlatillo(val){
		var array = this.state.platillosComanda;
		array.push(val);
		this.setState({platillosComanda: array});
	}

	eliminaPlatillo(index){
		var array = this.state.platillosComanda;
		array.splice(index, 1);
		this.setState({platillosComanda: array});
	}
	
	actualizaCantidad(index, int){
		var array = this.state.platillosComanda;
		array[index].cantidad = array[index].cantidad + int;
		this.setState({platillosComanda: array});
  	}
	
  	render() {
    	return (
		<Container className="has-navbar-fixed-top main">
			<ToastContainer />
			<Navbar className="is-fixed-top">
				<Navbar.Brand>
					<Navbar.Item>
						<img alt='LECO' src={process.env.PUBLIC_URL + '/leco.ico'}></img>
						<Content>
							<label className="has-text-weight-bold">Restaurante LECO</label>
						</Content>
					</Navbar.Item>
				</Navbar.Brand>
			</Navbar>
			<Tabs className="is-centered is-boxed">
				<li className={this.state.nueva ? "is-active":""}>
					<a onClick={(e) => this.changeTab(true)}>
						<i className="icon-tab">
							<img alt="Nueva Comanda" src={process.env.PUBLIC_URL + '/cart.png'}></img>
						</i> Nueva Comanda
					</a>
				</li>
				<li className={!this.state.nueva ? "is-active":""}>
					<a onClick={(e) => this.changeTab(false)}>
						<i className="icon-tab">
							<img alt="Lista Comandas" src={process.env.PUBLIC_URL + '/list.png'}></img>
						</i> Lista de Comandas
					</a>
				</li>
			</Tabs>
			<div className={!this.state.nueva ? "is-hidden":""}>
				<Menu 
					tipos={this.state.tipos} 
					categoriaSelected={this.state.categoriaSelected}
					platillosComanda={this.state.platillosComanda} 
					filtraCategoria={(e) => this.filtraCategoria(e)}
					enviaComanda={() => this.enviaComanda()}
					adicionaPlatillo={(val) => this.adicionaPlatillo(val)} 
					eliminaPlatillo={(val) => this.eliminaPlatillo(val)}
					actualizaCantidad={(val,i) => this.actualizaCantidad(val,i)}
					categorias={this.state.categorias} 
					menu={this.state.menu}></Menu>
			</div>
			<div className={this.state.nueva ? "is-hidden":""}>
				<Comandas ordenes={this.state.ordenes}></Comandas>
			</div>
		</Container>
    );
  }
}

export default Restaurante;
