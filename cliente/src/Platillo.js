import React, { Component } from 'react';
import { Columns , Card , Modal} from 'react-bulma-components/full';
import CurrencyFormat from 'react-currency-format';

import './Platillo.css';

class PlatilloModal extends Component {
  
  constructor(props) {
    super(props);
    this.state= {
      cantidad:1, 
      nuevoPlatillo : props.nuevoPlatillo,
      opciones : '',
      comentario : ''
    };
    this.changeComentario = this.changeComentario.bind(this);
  }

  opciones =[];

  submit(e){
    e.preventDefault();
    var platilloComanda = {
      timestamp : Math.floor(Date.now() / 1000),
      cantidad : this.state.cantidad,
      platillo : this.props.platillo,
      comentario : this.state.comentario,
      opciones : this.state.opciones
    };
    this.state.nuevoPlatillo(platilloComanda);
    this.props.close();
  }

  changeOpcion(opcion, tipo, index){
    if(tipo === "radio"){
      this.setState({opciones : opcion});
    }
    if(tipo === "checkbox"){
      var opc = '';
      for (let i = 0; i < this.opciones.length; i++) {
        if(this.opciones[i].checked){
          opc = opc + this.opciones[i].value + ',';
        }
      }
      opc = opc.length > 0 ? opc.substring(0, opc.length - 1):opc;
      console.log(opc);
      this.setState({opciones : opc});
    }
  }

  changeComentario(event) {
    this.setState({comentario: event.target.value});
  }

  render() {
    return (
        <Modal show={this.props.visible} onClose={this.props.close}>
            <Modal.Card>
              <form onSubmit={(e) => this.submit(e)} ref="form">
                <Modal.Card.Head>
                    <p className="titulo-modal">{this.props.platillo.nombre}</p>
                </Modal.Card.Head>
                <Modal.Card.Body>
                    <p className="descripcion-modal">{this.props.platillo.descripcion}</p>
                    {this.props.platillo.tituloOpciones  &&
                        <div>
                            <h2 className="titulo-opciones-modal">{this.props.platillo.tituloOpciones}</h2>
                            {this.props.platillo.opciones.map((opcion, index) => {
                                return <div className="is-block" key={opcion}>
                                <label className={this.props.platillo.tipoOpciones}>
                                    <input type={this.props.platillo.tipoOpciones} 
                                        value={opcion}
                                        name={this.props.platillo._id}
                                        ref={input => this.opciones[index] = input}
                                        onChange={() => this.changeOpcion(opcion,this.props.platillo.tipoOpciones, index)}
                                        required={this.props.platillo.tipoOpciones === 'radio'}></input>
                                    {opcion}
                                </label>
                                </div>
                            })}
                        </div>
                    }
                    <div>
                      <h2 className="titulo-comentario-modal">Comentarios</h2>
                      <textarea className="textarea" onChange={this.changeComentario} 
                        value={this.state.comentario}></textarea>
                    </div>
                </Modal.Card.Body>
                <Modal.Card.Foot>
                  <button className="button" type="button" disabled={this.state.cantidad === 1} 
                    onClick={() => this.setState({cantidad: this.state.cantidad - 1})}>-</button>
                  {this.state.cantidad}
                  <button className="button" type="button" 
                    onClick={() => this.setState({cantidad: this.state.cantidad + 1})}>+</button>
                  <button className="button is-success is-pulled-left" 
                    type="submit">Agregar a la comanda</button>
                </Modal.Card.Foot>
              </form>
            </Modal.Card>
        </Modal>
    );
  }
}

class Platillo extends Component {
  
  constructor(props) {
    super(props);
    this.state= {selected:false, nuevoPlatillo : props.nuevoPlatillo};
    this.platillo = props.platillo;
    this.comandaPlatillo = props.comandaPlatillo;
    this.changeSelected = this.changeSelected.bind(this);
  }

  changeSelected(event) {
    this.setState({
      selected: !this.state.selected
    });
  }

  render() {
      return (
        <Columns.Column size="one-quarter">
          <span className="platillo" onClick={this.changeSelected}>
            <Card>
              <Card.Header>
                <Card.Header.Title className="platillo-titulo">
                  {this.platillo.nombre}
                </Card.Header.Title>
              </Card.Header>
              <Card.Content>
                <p className="platillo-descripcion">{this.platillo.descripcion}</p>
                <data className="has-text-weight-bold is-block">
                  <CurrencyFormat value={this.platillo.costo} displayType={'text'} 
                  thousandSeparator={true} prefix={'$'} decimalScale={2} fixedDecimalScale={true} />
                </data>
              </Card.Content>
            </Card>
          </span>
          <PlatilloModal visible={this.state.selected} 
            close={this.changeSelected} platillo={this.platillo} 
            nuevoPlatillo={(val) => this.state.nuevoPlatillo(val)}></PlatilloModal>
        </Columns.Column>
      );
  }
}

export default Platillo;
