import React, { Component } from 'react';
import { Columns  } from 'react-bulma-components/full';

import Platillo from './Platillo.js';
import Comanda from './Comanda';

import './Menu.css';

class Tipo extends Component{
  render(){
    return(
      <div key={this.props.tipo}>
        <p className="is-size-4 cat-title is-block">{this.props.tipo}</p>
        <Columns className="is-flex">
          {this.props.platillos.map(platillo => 
            <Platillo key={platillo._id} platillo={platillo} 
              nuevoPlatillo={(val) => this.props.adicionaPlatillo(val)}/>
            )}
        </Columns>
      </div>
    );
  }
}

class Categoria extends Component{
  render() {
    return (
      <div key={this.props.nombre}
        className={this.props.categoriaSelected === this.props.nombre? '':'is-hidden'}>
        <p className="cat-title has-text-centered has-text-weight-bold is-size-4">Menú {this.props.nombre}</p>
        {Object.entries(this.props.tipos).map((tipo,index) => {
          return <Tipo key={tipo[0]} tipo={tipo[0]} platillos={tipo[1]} 
          adicionaPlatillo={(val => this.props.adicionaPlatillo(val))}></Tipo>
        })}
      </div>
    );
  }
}

class Menu extends Component {

  filtraCategoria(e){
    this.props.filtraCategoria(this.categoriaSelected.value);
  }

  render() {
    return (
      <Columns>
        <Columns.Column className="is-two-thirds">
          <div>
            <div className="select is-pulled-right">
              <select onChange={this.filtraCategoria.bind(this)} 
                ref={select => this.categoriaSelected = select} name="categoria">
                {this.props.categorias.map(function(categoria){
                  return <option key={categoria}>{categoria}</option>
                })}
              </select>
            </div>
            {Object.entries(this.props.menu).map((categoria,index) => {
              return <Categoria key={categoria[0]} nombre={categoria[0]} tipos={categoria[1]} 
              categoriaSelected={this.props.categoriaSelected}
              adicionaPlatillo={(val) => this.props.adicionaPlatillo(val)}></Categoria>
            })}
          </div>
        </Columns.Column>
        <Columns.Column className="is-one-third">
          <Comanda platillos={this.props.platillosComanda} viewOnly={false}
            enviaComanda={() => this.props.enviaComanda()}
            eliminaPlatillo={(i) => this.props.eliminaPlatillo(i)}
            actualizaCantidad={(p,i) => this.props.actualizaCantidad(p,i)}></Comanda>
        </Columns.Column>
      </Columns>
    );
  }
}

export default Menu;
