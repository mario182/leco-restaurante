import React from 'react';
import ReactDOM from 'react-dom';
import Restaurante from './Restaurante';

import './index.css';

ReactDOM.render(<Restaurante />, document.getElementById('root'));
