import React, { Component } from 'react';
import { Box, Columns} from 'react-bulma-components/full';
import CurrencyFormat from 'react-currency-format';

class PlatilloComanda extends Component{
    render(){
        return(
            <Box className="is-marginless">
                <Columns className="is-gapless">
                    <Columns.Column className="is-three-fifths has-text-left">
                        <label className="is-block">{this.props.platilloComanda.platillo.nombre}</label>
                        <label className="is-block">{this.props.platilloComanda.opciones}</label>
                        { !this.props.viewOnly &&
                            <button className="button is-text is-white has-text-success is-paddingless"
                            onClick={this.props.eliminaPlatillo}>Eliminar</button>
                        }
                    </Columns.Column>
                    <Columns.Column className="is-two-fifths">
                        <Columns.Column className="is-full has-text-right">
                            { !this.props.viewOnly &&
                                <button className="button is-success is-small is-outlined is-rounded" type="button" 
                                disabled={this.props.platilloComanda.cantidad === 1} 
                                onClick={() => this.props.actulizaCantidad(-1)}>-</button>
                            }
                            <label>{this.props.platilloComanda.cantidad}</label>
                            { !this.props.viewOnly &&
                                <button className="button is-success is-small is-outlined is-rounded" type="button" 
                                onClick={() => this.props.actulizaCantidad(1)}>+</button>
                            }
                        </Columns.Column>
                        <Columns.Column className="is-full has-text-right">
                            <label className="has-text-weight-bold">
                                <CurrencyFormat 
                                value={this.props.platilloComanda.cantidad * this.props.platilloComanda.platillo.costo} 
                                displayType={'text'} thousandSeparator={true} prefix={'$'} decimalScale={2} 
                                fixedDecimalScale={true} />
                            </label>
                        </Columns.Column>
                    </Columns.Column>
                </Columns>
            </Box>
        );
    }
}

class Comanda extends Component {
    render() {
        return (
            <Box className="has-text-centered">
                <Box className="is-paddingless is-inline-block">
                    {!this.props.viewOnly  &&
                        <button className="is-inline-block is-link button"
                        onClick={this.props.enviaComanda}
                        disabled={this.props.platillos.length === 0} >Generar Comanda</button>
                    }
                    {this.props.viewOnly  &&
                        <h1 className="title is-3">Resumen de Comanda</h1>
                    }
                </Box>
                <div>
                    {this.props.platillos.map((platilloComanda, index) =>{ 
                        return <PlatilloComanda key={platilloComanda.timestamp} 
                            platilloComanda={platilloComanda} viewOnly={this.props.viewOnly}
                            eliminaPlatillo={() => this.props.eliminaPlatillo(index)}
                            actulizaCantidad={(i) => this.props.actualizaCantidad(index,i)}>
                            </PlatilloComanda>;                        
                    })}
                </div>
                <Box>
                    <Columns className="is-full">
                        <Columns.Column className="is-two-quartes has-text-left">
                            <label className="has-text-weight-bold">Total:</label>
                        </Columns.Column>
                        <Columns.Column className="is-two-quartes has-text-right">
                            <label className="has-text-weight-bold">
                                <CurrencyFormat 
                                value={calculaTotal(this.props.platillos)} 
                                displayType={'text'} thousandSeparator={true} prefix={'$'} decimalScale={2} 
                                fixedDecimalScale={true} />
                            </label>
                        </Columns.Column>
                    </Columns>
                </Box>
            </Box>
        );
    }
}

function calculaTotal(array){
    var total = 0;
    for (let i = 0; i < array.length; i++) {
        total = total + (array[i].cantidad * array[i].platillo.costo);
    }
    return total;
}

export default Comanda;