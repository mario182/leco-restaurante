import React, { Component } from 'react';
import { Columns, Box  } from 'react-bulma-components/full';
import CurrencyFormat from 'react-currency-format';

import Comanda from './Comanda';

import './Comandas.css';


class Comandas extends Component {

  constructor() {
    super();
    this.state = { comandaSeleccionada:[], comandaIndex:-1};
  }

  actualizaSeleccion(index){
    var comandaSeleccionada = this.props.ordenes[index];
    this.setState({comandaSeleccionada: comandaSeleccionada.platillos, comandaIndex: index});
  }

  render() {
    return (
      <Columns>
        <Columns.Column className="is-two-thirds">
            {this.props.ordenes.map((comanda, index) => {
                return <Columns.Column key={comanda.fecha} className="is-one-quarter is-inline-block">
                    <Box className={this.state.comandaIndex === index ? "comanda-creada seleccionada":"comanda-creada"}>
                        <div onClick={()=> this.actualizaSeleccion(index)}>
                        <label className="is-block has-text-centered has-text-weight-bold">Comanda {index + 1}</label>
                        <label className="is-block">{timestampToDate(comanda.fecha*1000)}</label>
                        <label className="is-block has-text-right has-text-weight-bold"><CurrencyFormat value={calculaTotal(comanda.platillos)} displayType={'text'} 
                        thousandSeparator={true} prefix={'$'} decimalScale={2} fixedDecimalScale={true} /></label>
                        </div>
                    </Box>
                </Columns.Column>
            })}
        </Columns.Column>
        <Columns.Column className="is-one-third">
            <Comanda platillos={this.state.comandaSeleccionada} 
                viewOnly={true}
                enviaComanda={null}
                eliminaPlatillo={null}
                actualizaCantidad={null}></Comanda>
        </Columns.Column>
      </Columns>
    );
  }
}

function timestampToDate(timestamp){
    var dateFormat = require('dateformat');
    var t = new Date(timestamp);
    return dateFormat(t,"dd/mm/yy HH:MM");
}

function calculaTotal(array){
    var total = 0;
    for (let i = 0; i < array.length; i++) {
        total = total + (array[i].cantidad * array[i].platillo.costo);
    }
    return total;
}

export default Comandas;
