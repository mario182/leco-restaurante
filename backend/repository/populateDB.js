var mongoose = require('mongoose')
var fs = require('fs');
var propertiesReader = require('properties-reader');

var properties = propertiesReader('application.properties');

var Platillo = require('../model/platillo.js');

var files = properties.get('data.files').split(",");

var cargarPlatillos =  function(){
	for(var file in files){
		var data = JSON.parse(fs.readFileSync(files[file]));
		for(platillo in data){
			var object = data[platillo];
			Platillo.updateOne({nombre : object.nombre}, object, {upsert: true, runValidators: true}).exec();
		}
	}
}

module.exports = {
	cargar : cargarPlatillos
}			