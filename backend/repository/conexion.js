var mongoose = require('mongoose');
var propertiesReader = require('properties-reader');

var properties = propertiesReader('./application.properties');

var database = 'mongodb://'+ properties.get('database.host') +':'+ properties.get('database.port') +'/' + properties.get('database.dbName');
mongoose.connect(database, { useCreateIndex: true, useNewUrlParser: true });

mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on('error', function(err){
  if(err) throw err;
});

db.once('open', function callback () {
  console.info('Conectado con exito a MongoDB.');
});

module.exports = db;