/**
* 
*	Controla el inicio del servidor y las peticiones que recibe.
*
*/
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var db = require('./repository/conexion.js');
var populateDB = require('./repository/populateDB.js');

var platilloControlador = require('./controladores/platilloControlador.js');
var ordenControlador = require('./controladores/ordenControlador.js');

var app = express();

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'build')));

app.get('/menu', (req,res) => {
	platilloControlador.obtener().then(function(data) {
		res.send(data);
	});
});

app.get('/ordenes', (req,res) => {
	ordenControlador.obtener().then(function(data){
		res.send(data);
	});
});

app.post('/crearOrden', (req,res) => {
	ordenControlador.salvar(req.body).then(function(data){
		res.send(data);
	});
});

app.delete('/borrarOrden', (req,res) => {
	ordenControlador.borrar(req.body).then(function(data){
		res.send(data);
	});
});

app.get('*', (req,res) => {
	res.sendFile(path.join(__dirname, 'build/index.html'));
});

/**
* Inicia el servidor en el puerto 8080.
* 
*/
var server = app.listen(8080, function () {
	populateDB.cargar()
	console.log("Servidor iniciado.");
})
