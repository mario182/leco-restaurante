var Platillo = require('../model/platillo.js');
var propertiesReader = require('properties-reader');

var properties = propertiesReader('application.properties');

var obtenerPlatillos =  function(){
	return new Promise(function(resolve, reject){
		var result = new Object();

		result.categorias = properties.get('menu.categorias').split(",");
		result.tipos = properties.get('menu.tipos').split(",");
		result.menu = new Object();

		Platillo.find({}, null, {sort : {categoria: 1, tipo: 1}}, function(err, docs){
			if (err) {
		     	reject(err);
		    }
		    for(var platillo in docs) {
		    	var o = docs[platillo];
		    	if(!result.menu.hasOwnProperty(o.categoria))
		    		result.menu[o.categoria] = new Object();
		    	if(!result.menu[o.categoria].hasOwnProperty(o.tipo))
		    		result.menu[o.categoria][o.tipo] = [];
		    	result.menu[o.categoria][o.tipo].push(o);
		    }
		    resolve(result);
		});
	});
};

module.exports = {
	obtener : obtenerPlatillos
}