var Orden = require('../model/orden.js');

var obtenerOrdenes =  function(){
	return Orden.find({}, null, {sort : {fecha: -1}}).populate('platillos.platillo').exec();
};

var salvarOrden =  function(orden){
	return Orden.create(orden);
};

var borrarOrden = function(orden){
	return Orden.findById(orden._id).deleteOne().exec();
};

module.exports = {
	obtener : obtenerOrdenes,
	salvar : salvarOrden,
	borrar : borrarOrden
}