var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var OrdenSchema = new Schema({
    fecha: Number,
    platillos: [{
        timestamp: Number,
        cantidad: {
            type: Number,
            min: 1,
            required: true
        },
        platillo: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Platillo'
        },
        opciones:String,
        comentario: String
    }]
});

var Orden = mongoose.model('Orden', OrdenSchema );

module.exports = Orden;