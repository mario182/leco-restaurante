var mongoose = require('mongoose');
var propertiesReader = require('properties-reader');

var properties = propertiesReader('application.properties');
var Schema = mongoose.Schema;

var PlatilloSchema = new Schema({
    nombre: { type: String, required: true, unique: true },
    costo:  {
        type: Number,
        min: [0, 'Los platillos no son gratuitos!'],
    	required: true
    },
    descripcion: String,
    tipo: {
    	type: String,
        enum: properties.get('menu.tipos').split(",")
    },
    categoria: {
    	type: String,
        enum: properties.get('menu.categorias').split(",")
    },
    tituloOpciones: String,
    tipoOpciones: {
        type: String,
        enum: ['checkbox', 'radio']
    },
    opciones: [String]
});

var Platillo = mongoose.model('Platillo', PlatilloSchema );

module.exports = Platillo;